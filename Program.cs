﻿using System.Linq;

namespace Icod.GenKey {

	public static class GenKey {

		#region nested classes
		private sealed class Pair<S, T> {
			private readonly S myFirst;
			private readonly T mySecond;
			public Pair( S first, T second ) : base() {
				myFirst = first;
				mySecond = second;
			}
			public S First {
				get {
					return myFirst;
				}
			}
			public T Second {
				get {
					return mySecond;
				}
			}
		}
		#endregion nested classes

		private const System.Int32 theDefaultKeyLen = 32;

		[System.STAThread]
		public static System.Int32 Main( System.String[] args ) {

			var cmd = new Pair<System.Int32, System.Boolean>( theDefaultKeyLen, false );
			if ( ( args ?? new System.String[ 0 ] ).Any() ) {
				try {
					cmd = ParseInput( args );
					if ( null == cmd ) {
						PrintUsage();
						return 1;
					}
				} catch ( System.Exception ) {
					PrintUsage();
					return 1;
				}
			}

			var len = cmd.First;
			if ( len < 1 ) {
				PrintUsage();
				return 1;
			}
			var data = new System.Byte[ len ];
			using ( var provider = new System.Security.Cryptography.RNGCryptoServiceProvider() ) {
				provider.GetBytes( data );
			}
			var builder = new System.Text.StringBuilder( len << 2 );
			foreach ( var b in data ) {
				builder = builder.AppendFormat( "{0:x2}", b );
			}
			var text = builder.ToString();
			if ( cmd.Second ) {
				System.Windows.Forms.Clipboard.SetText( text );
			}
			System.Console.Out.WriteLine( text );
			return 0;
		}

		private static Pair<System.Int32, System.Boolean> ParseInput( System.String[] args ) {
			var clip = false;
			var len = theDefaultKeyLen;
			System.String @switch = null;
			var i = -1;
			var length = args.Length - 1;
			do {
				@switch = args[ ++i ];
				if (
					( "--len".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) )
					|| ( "/len".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) )
				) {
					len = System.Convert.ToInt32( args[ ++i ] );
					if ( len < 1 ) {
						return null;
					}
				} else if (
					( "--clip".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) )
					|| ( "/clip".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) )
				) {
					clip = true;
				} else {
					return null;
				}
			} while ( i < length );
			return new Pair<System.Int32, System.Boolean>( len, clip );
		}

		private static void PrintUsage() {
			System.Console.Error.WriteLine( "No, no, no! Use it like this, Einstein:" );
			System.Console.Error.WriteLine( "GenKey.exe [/len length] [/clip]" );
			System.Console.Error.WriteLine( "The optional /len argument specifies the length of the key, in bytes." );
			System.Console.Error.WriteLine( "The default key length is 32 bytes." );
			System.Console.Error.WriteLine( "If present, the /clip argument specifies the output should also be copied to the clipboard." );
			System.Console.Error.WriteLine( "Example: GenKey.exe" );
			System.Console.Error.WriteLine( "Example: GenKey.exe /len 64" );
			System.Console.Error.WriteLine( "Example: GenKey.exe /len 128 /clip" );
			System.Console.Error.WriteLine( "Example: GenKey.exe /clip" );
		}

	}

}